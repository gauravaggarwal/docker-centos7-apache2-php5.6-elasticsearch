# docker-centos7-apache2-php5.6-elasticsearch
## Table of contents 

1. Installation
2. Check container services and run the project
3. Logging & Debugging
4. Summary
 
 
###Installation

1. [Install the docker first](https://docs.docker.com/install/)
2. [Install docker-compose](https://docs.docker.com/compose/install/)
3. git clone https://gauravaggarwal@bitbucket.org/gauravaggarwal/docker-centos7-apache2-php5.6-elasticsearch.git
4. cd docker-centos7-apache2-php5.6-elasticsearch
5. docker-compose build
6. docker-compose up -d

###Check container services and run the project

1. **docker ps** command will show the list of running images
2. **docker inspect literature-review-net** command get all the containers information including IP address
3. get the IP of "Name": "php5.6-apache" listed last of above command in my case is "IPv4Address": "172.27.0.4/16"
4. go to the browser and type **http://172.27.0.4** your apache page should be displayed 


###Logging & Debugging
  
1. docker logs {CONTAINER_ID} command will display the specific error related to the container
2. In case if elastic search service not up check the log, on my linux system it was stuck because of low virtual memory than i used "sudo sysctl -w vm.max_map_count=262144"
  

###Summary
  
  Created seperate files for centos and php under docker-files/ folder and making the centos as base image, Using port 6551 and 6552 its upto us to choose any open port.
  Overriding the httpd.conf file keeping inside docker-config/vhosts/ folder so if you want to customize the apache setting that we can define in this file, module re_write was not working so i added the below command and override the container configuration file.
  Include conf.modules.d/*.conf
  LoadModule rewrite_module modules/mod_rewrite.so